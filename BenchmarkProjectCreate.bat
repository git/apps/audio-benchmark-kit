@REM ******************************************************************************
@REM * FILE PURPOSE: Audio Benchmark Startup Project Creator
@REM ******************************************************************************
@REM * FILE NAME: BenchmarkProjectCreate.bat
@REM *
@REM * DESCRIPTION: 
@REM *  The script file is used to create the Audio Benchmark example projects of K2G
@REM *  & PRIMUS under PDK. These projects are available in the specified
@REM *  workspace.
@REM *
@REM * Syntax:
@REM *  BenchmarkProjectCreate.bat 
@REM *
@REM * Description:     (first option is default)
@REM *  soc         -   K2G / K2H/ K2E/ C6678/ C6657/ AM574x/ AM572X/ AM571x/ OMAPL138 / OMAPL137
@REM *  board       -   all 
@REM *  module      -   all (FFT / FIR / IIR)
@REM *
@REM * Example:
@REM *  a) BencmarkProjectCreate.bat
@REM *              - Creates all module projects for the K2G soc for DSP little endian
@REM *
@REM * Copyright (C) 2012-2017, Texas Instruments, Inc.
@REM *****************************************************************************
@echo OFF

REM Lists of valid input parameters - MUST be updated as new input parameters are added
REM *****************************************************************************
set soc_list=K2G K2H K2E C667x C665x AM571x AM572x AM574x OMAPL138 OMAPL137 C6747 C6748
set endian_list=little
set module_list= all FFT FIR IIR
set proc_list=dsp

REM Parameter Validation: Check if the argument was passed to the batch file.
REM *****************************************************************************
REM Argument [soc] is used to set SOC_ARG variable.
set tempVar1=%1
if not defined tempVar1 goto nosoc
set SOC_ARG=%tempVar1%
goto socdone
:nosoc
set SOC_ARG=K2G
:socdone

REM Argument [board] is used to set BOARD_ARG variable.
REM This is the board type of the soc for which project are to be created.
set tempVar2=%2
if not defined tempVar2 goto noboard
set BOARD_ARG=%tempVar2%
goto boarddone
:noboard
set BOARD_ARG=all
:boarddone

REM Argument [module] is used to set MODULE variable.
REM This is specific module for which projects are created.
REM Valid Values are LLD name. Defaults to all LLD's.
set tempVar3=%3
if not defined tempVar3 goto nomodule
set MODULE_ARG=%tempVar3%
goto moduledone
:nomodule
set MODULE_ARG=all
:moduledone

REM Argument [board] is used to set BOARD_ARG variable.
REM This is the board type of the soc for which project are to be created.
set PROCESSOR=dsp
set PDK_SHORT_NAME=%~sdp0

REM *****************************************************************************

echo =========================================================================
echo Configuration:
echo.   SOC             :   %SOC_ARG%
echo    BOARD           :   %BOARD_ARG%
echo.   MODULE          :   %MODULE_ARG%
echo.   PDK_SHORT_NAME  :   %PDK_SHORT_NAME%
echo =========================================================================
echo Checking Configuration...

for %%a in (%soc_list%) do (
    if %SOC_ARG% == %%a (
        goto end_soc_check
    )
)
echo ERROR: SOC (%SOC_ARG%) is invalid
goto CONFIG_ERR
:end_soc_check

if %SOC_ARG% == OMAPL138 (
	set CORE=c674
) else if %SOC_ARG% == OMAPL137 (
	set CORE=c674
) else if %SOC_ARG% == C6748 (
	set CORE=c674
) else if %SOC_ARG% == C6747 (
	set CORE=c674
) else (
	set CORE=c66
)

if %BOARD_ARG% == all (
    goto end_board_check
)
setlocal enabledelayedexpansion
set board_type=!BOARD_ARG:%SOC_ARG%=!
if not %board_type% == %BOARD_ARG% (
    goto end_board_check
)
echo ERROR: Board (%BOARD_ARG%) is invalid for the specified SOC(%SOC_ARG%)
goto CONFIG_ERR
:end_board_check

for %%a in (%module_list%) do (
    if %MODULE_ARG% == %%a (
        goto end_module_check
    )
)
echo ERROR: Module (%MODULE_ARG%) is invalid
goto CONFIG_ERR
:end_module_check

set ENDIAN=little

goto CONFIG_COMPLETE

:CONFIG_ERR
echo Exiting...
echo =========================================================================
goto ENDSCRIPT

:CONFIG_COMPLETE
echo Complete
echo =========================================================================

REM *****************************************************************************
REM * Version Information of the various tools etc required to build the test
REM * projects. Customers are free to modify these to meet their requirements.
REM *****************************************************************************

REM This is to control the CCS version specific project create command
REM Set to 'no' when using CCSv5 and CCSv6 or set to 'yes' when using CCSv4
set IS_CCS_VERSION_4=no

REM Set to 'no' when using QT, EVM, VDB, or other hardware. Set to 'yes' only when using the simulator.
set IS_SIMULATOR_SUPPORT_NEEDED=no

REM Install Location for CCS
set CCS_INSTALL_PATH=%TOOLS_INSTALL_PATH%/ccs930/ccs

REM Workspace where the PDK projects will be created.
set MY_WORKSPACE=%PDK_SHORT_NAME%\BenchmarkProjects

REM macros.ini location
set MACROS_FILE=%PDK_SHORT_NAME%\macros.ini

REM Version of DSPLIB
set DSPLIB_VERSION=3_4_0_4

REM Version of MATHLIB
set MATHLIB_VERSION=3_1_2_4

REM This is the format of the executable being created
REM Valid Values are 'ELF' and 'COFF'
set OUTPUT_FORMAT=ELF

if not defined PROC_SDK_INSTALL_PATH (
    set PROC_SDK_INSTALL_PATH=%SDK_INSTALL_PATH%/processor_sdk_rtos_%PROC_SDK_VERSION%
)
set PROC_SDK_INSTALL_PATH=%PROC_SDK_INSTALL_PATH:\=/%

set PDK_INSTALL_PATH =%SDK_INSTALL_PATH%/pdk_%PDK_SOC%_%PDK_VERSION%
set DSPLIB_INSTALL_PATH =%SDK_INSTALL_PATH%/dsplib_%CORE%x_%DSPLIB_VERSION%
set MATHLIB_INSTALL_PATH =%SDK_INSTALL_PATH%/mathlib_%CORE%x_%MATHLIB_VERSION%

goto PATH_COMPLETE

:PATH_ERR
echo Exiting...
echo =========================================================================
goto ENDSCRIPT

:PATH_COMPLETE
REM PDK Part Number & Platform name
if %SOC_ARG% == K2G (
    set PDK_PARTNO=66AK2G02
    set RTSC_PLATFORM_NAME=ti.platforms.evmTCI66AK2G02
	set RTSC_TARGET=ti.targets.elf.C66
	set CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC64xPlusDevice"
) else if %SOC_ARG% == OMAPL138 (
    set PDK_PARTNO=OMAPL138
    set RTSC_PLATFORM_NAME=ti.platforms.evm6748
   	set RTSC_TARGET=ti.targets.elf.C674
	set CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC674xDevice"
) else if %SOC_ARG% == OMAPL137 (
    set PDK_PARTNO=OMAPL137
    set RTSC_PLATFORM_NAME=ti.platforms.evm6747
   	set RTSC_TARGET=ti.targets.elf.C674
	set CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC674xDevice"
)  else if %SOC_ARG% == C6748 (
    set PDK_PARTNO=OMAPL138
    set RTSC_PLATFORM_NAME=ti.platforms.evm6748
   	set RTSC_TARGET=ti.targets.elf.C674
	set CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC674xDevice"
) else if %SOC_ARG% == C6747 (
    set PDK_PARTNO=OMAPL137
    set RTSC_PLATFORM_NAME=ti.platforms.evm6747
   	set RTSC_TARGET=ti.targets.elf.C674
	set CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC674xDevice"
) else if  %SOC_ARG% == AM574x (
    set PDK_PARTNO=AM574X
    set PDK_ECLIPSE_ID=com.ti.pdk.am57xx
    if %BOARD_ARG% == idkAM574x (
        set RTSC_PLATFORM_NAME=ti.platforms.idkAM572X
    )
    set RTSC_TARGET=ti.targets.elf.C66
	set CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC64xPlusDevice"
) else if  %SOC_ARG% == AM572x (
    set PDK_PARTNO=AM572X
    set PDK_ECLIPSE_ID=com.ti.pdk.am57xx
    if %BOARD_ARG% == idkAM572x (
        set RTSC_PLATFORM_NAME=ti.platforms.idkAM572X
    ) else (
        set RTSC_PLATFORM_NAME=ti.platforms.evmAM572X
    )
    set RTSC_TARGET=ti.targets.elf.C66
	set CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC64xPlusDevice"
) else if  %SOC_ARG% == AM571x (
    set PDK_PARTNO=AM571X
    set PDK_ECLIPSE_ID=com.ti.pdk.am57xx
    set RTSC_PLATFORM_NAME=ti.platforms.idkAM571X
    set RTSC_TARGET=ti.targets.elf.C66
	set CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC64xPlusDevice"
) else if  %SOC_ARG% == C667x (
    set PDK_PARTNO=C6678L
    set PDK_ECLIPSE_ID=com.ti.pdk.c667x
    set RTSC_PLATFORM_NAME=ti.platforms.evm6678
	set RTSC_TARGET=ti.targets.elf.C66
	set CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC64xPlusDevice"
) else if  %SOC_ARG% == C665x (
    set PDK_PARTNO=C6657
    set PDK_ECLIPSE_ID=com.ti.pdk.c665x
    set RTSC_PLATFORM_NAME=ti.platforms.evm6657
	set RTSC_TARGET=ti.targets.elf.C66
	set CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC64xPlusDevice"
) else (
    REM Need to exit the batch script cleanly 
	echo    SOC not Specified. Running Default setup
    set PDK_PARTNO=66AK2G02
    set RTSC_PLATFORM_NAME=ti.platforms.evmTCI66AK2G02
	set RTSC_TARGET=ti.targets.elf.C66
	set CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC64xPlusDevice"
)


REM RTSC Target 
REM - Please ensure that you select this taking into account the
REM   OUTPUT_FORMAT and the RTSC_PLATFORM_NAME 

echo    PDK_PARTNO         : %PDK_PARTNO%
echo    PDK_ECLIPSE_ID     : %PDK_ECLIPSE_ID%
echo    RTSC_PLATFORM_NAME : %RTSC_PLATFORM_NAME%
echo.   RTSC_TARGET        : %RTSC_TARGET%
echo.   CCS_DEVICE         : %CCS_DEVICE%
REM *****************************************************************************
REM *****************************************************************************
REM                 Please do NOT change anything below this
REM *****************************************************************************
REM *****************************************************************************

REM Set auto create command by default for use with CCSv5 and CCSv6
set AUTO_CREATE_COMMAND=eclipse\eclipsec -noSplash

REM If is CCS version 4 then set auto create command for use with CCSv4
if .%IS_CCS_VERSION_4% == .yes set AUTO_CREATE_COMMAND=eclipse\jre\bin\java -jar %CCS_INSTALL_PATH%\eclipse\startup.jar

REM Set project for Silicon or QT by default
set SIMULATOR_SUPPORT_DEFINE=

REM If simulator support is needed then set the define
if .%IS_SIMULATOR_SUPPORT_NEEDED% == .yes set SIMULATOR_SUPPORT_DEFINE=-ccs.setCompilerOptions "--define SIMULATOR_SUPPORT"

REM Goto the PDK Installation Path.

pushd %PDK_SHORT_NAME%

echo *****************************************************************************
echo Detecting all projects in PDK and importing them in the workspace %MY_WORKSPACE%

REM Set BOARD to equal the SOC if BOARD is set to "all".  All projects for the
REM SOC will be picked up
if %BOARD_ARG% == all set BOARD_ARG=%SOC_ARG%
REM Set MODULE to null string so it picks up all projects of PROCESSOR
if %MODULE_ARG% == all set MODULE_ARG=""

if %PROCESSOR% == dsp (
    REM Search for all the dsp test Project Files in the PDK.
    for /F %%I IN ('dir /b /s Benchmark*%MODULE_ARG%*%BOARD_ARG%*%CORE%*roject.txt') do (
        set project_detected = 1

        echo Detected Test Project: %%~nI

        REM Goto each directory where the test project file is located and create the projects.
        pushd %%~dI%%~pI

        REM Execute the command to create the project using the parameters specified above.
        %CCS_INSTALL_PATH%\%AUTO_CREATE_COMMAND% -data %MY_WORKSPACE% -application com.ti.ccstudio.apps.projectCreate -ccs.name %%~nI -ccs.outputFormat %OUTPUT_FORMAT% -ccs.device %CCS_DEVICE% -ccs.endianness %ENDIAN% -ccs.kind executable -ccs.rts libc.a -ccs.args %%~nI%%~xI %SIMULATOR_SUPPORT_DEFINE% -ccs.overwrite full

        echo Copying macros.ini
        copy %MACROS_FILE% %MY_WORKSPACE%\%%~nI\macros.ini
		echo DSPLIB_INSTALL_PATH    = %SDK_INSTALL_PATH%/dsplib_%CORE%x_%DSPLIB_VERSION% >> %MY_WORKSPACE%\%%~nI\macros.ini
		echo MATHLIB_INSTALL_PATH   = %SDK_INSTALL_PATH%/mathlib_%CORE%x_%MATHLIB_VERSION% >> %MY_WORKSPACE%\%%~nI\macros.ini
		echo PDK_INSTALL_PATH   	= %SDK_INSTALL_PATH%/pdk_%PDK_SOC%_%PDK_VERSION%/packages >> %MY_WORKSPACE%\%%~nI\macros.ini
		echo PROC_SDK_INSTALL_PATH   = %PROC_SDK_INSTALL_PATH% >> %MY_WORKSPACE%\%%~nI\macros.ini
        popd
    )
)
popd

if not defined project_detected (
    echo No projects detected
)

echo Project generation complete
echo *****************************************************************************

:ENDSCRIPT
