ReadMe: 
Description of the directory structure of the package.

########################################################################################
Directory Structure:
########################################################################################

    * bin - directory contains prebuilt out files to run the benchmarks.
        sd_card_files: SD card boot files to run the benchmarks using SD boot. 
    * docs - directory contains ReadMe, Quick start guide and the software manifest for the package.
    * scripts - directory contains .txt script files that is used by BenchmarkProjectCreate script to create CCS projects
    * src
        + common - Contains linker command file and logging functions used by all benchmark tests.
        + singlePrecision_FFT - Source files for benchmark app for FFT
        + singlePrecision_FIR - Source files for benchmark app for FIR
        + singlePrecision_IIR - Source files for benchmark app for IIR 
	* BenchmarkProjectCreate.bat: Script used to generate CCS projects for the benchmarks.
	* create_sd.bat : This script is used to convert .out to SD boot table images.

For detailed documentation to build and run this software, please refer to 
http://processors.wiki.ti.com/index.php/Processor_SDK_RTOS_Audio_Benchmark_Starterkit 