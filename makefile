#*******************************************************************************
#* FILE PURPOSE: Top level makefile for Creating Component Libraries and example 
#*               binaries
#*******************************************************************************
#* FILE NAME: makefile
#*
#* DESCRIPTION: Defines Compiler tools paths, libraries , Build Options 
#*
#*
#*******************************************************************************
#*
# (Mandatory) Specify where various tools are installed.

ifeq ($(RULES_MAKE), )
include $(PDK_INSTALL_PATH)/ti/build/Rules.make
else
include $(RULES_MAKE)
endif

ifndef MAKE
export MAKE = make
endif

ifndef ECHO
export ECHO = echo
endif

ifndef RM
export RM = rm -f
endif

ifndef CP
export CP = cp
endif

ifndef SOC
SOC=k2g
endif

all: fft fir iir 
clean: fft_clean fir_clean iir_clean 

help:
	@echo "Standard Targets:"
	@echo "    help      - Prints all available target information"
	@echo "    all       - Builds all Component targets"
	@echo "    clean     - Cleans all Component targets"
	@echo ""
	@echo "Targets:"
	@echo "    fft		- Builds FFT benchmark test"
	@echo "    fir		- Builds FIR benchmark test"
	@echo "    iir		- Builds IIR benchmark test"
	@echo "    <alg>_clean - Cleans all targets within the IPC component"
	@echo "                  where <alg> is fft, fir, iir, etc"
	@echo ""
	@echo "     Add BUILD=CCS to target to redirect test output to CCS IO console"
	@echo ""
	@echo "NOTE: Instructions for rebuilding targets"
	@echo "      assumes Processor SDK build environment has been setup"
	
fft:
	$(MAKE) -C ./src/singlePrecision_FFT SOC=$(PDK_SOC) all

fir:
	$(MAKE) -C ./src/singlePrecision_FIR SOC=$(PDK_SOC) all
	
iir:
	$(MAKE) -C ./src/singlePrecision_IIR SOC=$(PDK_SOC) all
	
fft_clean:
	$(MAKE) -C ./src/singlePrecision_FFT SOC=$(PDK_SOC) clean

fir_clean:
	$(MAKE) -C ./src/singlePrecision_FIR SOC=$(PDK_SOC) clean

iir_clean:
	$(MAKE) -C ./src/singlePrecision_IIR SOC=$(PDK_SOC) clean