-ccs.linkFile "PROC_SDK_INSTALL_PATH/demos/audio-benchmark-starterkit/src/singlePrecision_FIR/DSPF_sp_fir_cplx_d.c" 
-ccs.linkFile "PROC_SDK_INSTALL_PATH/demos/audio-benchmark-starterkit/src/common/Benchmark_log.c" 
-ccs.linkFile "PROC_SDK_INSTALL_PATH/demos/audio-benchmark-starterkit/src/common/lnk_c66xx.cmd"
-ccs.setCompilerOptions " -mv6600 -g -DDEVICE_C6678 -dSOC_C6678 -dEVM_C6678 -DIO_CONSOLE --diag_warning=225 --display_error_number --diag_wrap=off --mem_model:data=far --debug_software_pipeline -k -k -I${PROC_SDK_INSTALL_PATH}/demos/audio-benchmark-starterkit/src/common -I${PDK_INSTALL_PATH} -I${PDK_INSTALL_PATH}/ti/csl -I${PDK_INSTALL_PATH}/ti/board/src/evmC6678/include -I${DSPLIB_INSTALL_PATH}/packages -I${MATHLIB_INSTALL_PATH}/packages -I${DSPLIB_INSTALL_PATH}/packages/ti/dsplib/src/common/c66 -I${DSPLIB_INSTALL_PATH}/packages/ti/dsplib/src/DSPF_sp_fir_cplx -I${DSPLIB_INSTALL_PATH}/packages/ti/dsplib/src/DSPF_sp_fir_cplx/c66 "
-ccs.setLinkerOptions " -I${DSPLIB_INSTALL_PATH}/packages -I${MATHLIB_INSTALL_PATH}/packages -I${DSPLIB_INSTALL_PATH}/packages/ti/dsplib/src/common/c66 -I${DSPLIB_INSTALL_PATH}/packages/ti/dsplib/src/DSPF_sp_fir_cplx -I${DSPLIB_INSTALL_PATH}/packages/ti/dsplib/src/DSPF_sp_fir_cplx/c66 -l${PDK_INSTALL_PATH}/ti/csl/lib/c6678/c66/release/ti.csl.ae66 -l${PDK_INSTALL_PATH}/ti/csl/lib/c6678/c66/release/ti.csl.intc.ae66 -l${PDK_INSTALL_PATH}/ti/osal/lib/nonos/c6678/c66/release/ti.osal.ae66 -l${PDK_INSTALL_PATH}/ti/board/lib/evmC6678/c66/release/ti.board.ae66 -l${PDK_INSTALL_PATH}/ti/drv/uart/lib/c6678/c66/release/ti.drv.uart.ae66"



