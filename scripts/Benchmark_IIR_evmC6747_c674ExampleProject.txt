-ccs.linkFile "PROC_SDK_INSTALL_PATH/demos/audio-benchmark-starterkit/src/singlePrecision_IIR/TISigCascade32f_2c_3s_kernel_cn.c" 
-ccs.linkFile "PROC_SDK_INSTALL_PATH/demos/audio-benchmark-starterkit/src/singlePrecision_IIR/TISigCascadeBiquad32f_2c_3s.h"
-ccs.linkFile "PROC_SDK_INSTALL_PATH/demos/audio-benchmark-starterkit/src/singlePrecision_IIR/TISigCascadeBiquad32f_2c_3s_d.c"
-ccs.linkFile "PROC_SDK_INSTALL_PATH/demos/audio-benchmark-starterkit/src/singlePrecision_IIR/TISigCascadeBiquad32f_2c_3s_kernel.c"
-ccs.linkFile "PROC_SDK_INSTALL_PATH/demos/audio-benchmark-starterkit/src/common/Benchmark_log.c" 
-ccs.linkFile "PROC_SDK_INSTALL_PATH/demos/audio-benchmark-starterkit/src/common/lnk_c674x.cmd"
-ccs.setCompilerOptions "-mv6740 -o3 --abi=eabi -g --vectypes=on -DDEVICE_OMAPL137 -DSOC_OMAPL137 -DevmOMAPL137 -DIO_CONSOLE -Dti_targets_C674 --diag_warning=225 --display_error_number --diag_wrap=off --mem_model:data=far --debug_software_pipeline -k -I${PROC_SDK_INSTALL_PATH}/demos/audio-benchmark-starterkit/src/common/ -I${PDK_INSTALL_PATH} -I${PDK_INSTALL_PATH}/ti/csl -I${PDK_INSTALL_PATH}/ti/board/src/evmOMAPL137/include"
-ccs.setLinkerOptions "-I${DSPLIB_INSTALL_PATH}/packages -I${MATHLIB_INSTALL_PATH}/packages -l${PDK_INSTALL_PATH}/ti/csl/lib/omapl137/c674/release/ti.csl.ae674 -l${PDK_INSTALL_PATH}/ti/csl/lib/omapl137/c674/release/ti.csl.intc.ae674 -l${PDK_INSTALL_PATH}/ti/osal/lib/nonos/omapl137/c674/release/ti.osal.ae674 -l${PDK_INSTALL_PATH}/ti/board/lib/evmOMAPL137/c674/release/ti.board.ae674 -l${PDK_INSTALL_PATH}/ti/drv/uart/lib/omapl137/c674/release/ti.drv.uart.ae674"

