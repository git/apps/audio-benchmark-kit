ReadMe:
This folder contains source files for common helper functions used by all the benchmark tests.  
It also provides linker command files that defines memory sections where the code and data used in the benchmark tests.  


For detailed documentation to build and run this software, please refer to 
http://processors.wiki.ti.com/index.php/Processor_SDK_RTOS_Audio_Benchmark_Starterkit 