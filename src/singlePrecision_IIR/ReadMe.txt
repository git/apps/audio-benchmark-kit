ReadMe:
This folder contains source and build files for FIR benchmarks. Users can build the benchmark test by using the makefile provided or
by using CCS projects created by BenchmarkProjectCrate.bat file.    


For detailed documentation to build and run this software, please refer to 
http://processors.wiki.ti.com/index.php/Processor_SDK_RTOS_Audio_Benchmark_Starterkit 