/* ======================================================================== *
 * TISigLib -- TI Signal Processing Floating-Point Math Function Library    *
 *                                                                          *
 *                                                                          *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/   *
 *                                                                          *
 *                                                                          *
 *  Redistribution and use in source and binary forms, with or without      *
 *  modification, are permitted provided that the following conditions      *
 *  are met:                                                                *
 *                                                                          *
 *    Redistributions of source code must retain the above copyright        *
 *    notice, this list of conditions and the following disclaimer.         *
 *                                                                          *
 *    Redistributions in binary form must reproduce the above copyright     *
 *    notice, this list of conditions and the following disclaimer in the   *
 *    documentation and/or other materials provided with the                *
 *    distribution.                                                         *
 *                                                                          *
 *    Neither the name of Texas Instruments Incorporated nor the names of   *
 *    its contributors may be used to endorse or promote products derived   *
 *    from this software without specific prior written permission.         *
 *                                                                          *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     *
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR   *
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT    *
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,   *
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT        *
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,   *
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY   *
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT     *
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    *
 * ======================================================================== */

/* ===========================================================================*/
/* tisigCascade32f_2c_3s_kernel_cn.c - single precision floating point sqrt */
/* Author: Asheesh Bhardwaj										              */
/* ===========================================================================*/
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include "c6x.h"
#include "c6x_vec.h"
#include "TISigCascadeBiquad32f_2c_3s.h"


int tisigCascadeBiquad32f_2c_3skernel_cn(CascadeBiquad_FilParam *pParam)
{
    float *restrict in1, *restrict in2, *restrict out1, *restrict out2;
    float input1, input2, output01, output02, output03, output11, output12, output13;

    float *restrict filtCfs ; /* Coeff ptrs */
    float *restrict filtVars_1, *restrict filtVars_2; /* Filter var mem ptr */
    int count, samp; /* Loop counters */

    float b11, b12, a11, a12;  /* Cascade-1 coeffs   */
    float b21, b22, a21, a22;  /* Cascade-2 coeffs   */
    float b31, b32, a31, a32;  /* Cascade-3 coeffs   */
    float vd00, vd01, vd02, vd03, vd04, vd05, vd10, vd11, vd12, vd13, vd14, vd15;

    float t0, t1, t2, t3, t4, t5;/* Filter temp regs  */

    count = pParam->sampleCount; /* I/p sample block-length */


    in1 = (float *)pParam->pin1;
	in2 = (float *)pParam->pin2;

	out1 = (float *)pParam->pOut1;
	out2 = (float *)pParam->pOut2;


    filtVars_1 = (float *)pParam->pVar0; /* Get filter var ptr */
    filtVars_2 = (float *)pParam->pVar1; /* Get filter var ptr */
    filtCfs = (float *)pParam->pCoef; /* Feed-forward coeff. ptr */

    b11 = filtCfs[0];
    b12 = filtCfs[1];
    a11 = filtCfs[2];
    a12 = filtCfs[3];

    b21 = filtCfs[4];
    b22 = filtCfs[5];
    a21 = filtCfs[6];
    a22 = filtCfs[7];

    b31 = filtCfs[8];
    b32 = filtCfs[9];
    a31 = filtCfs[10];
    a32 = filtCfs[11];

    /* Get the filter states into corresponding regs */

    vd00 = filtVars_1[0];
    vd01 = filtVars_1[1];
    vd02 = filtVars_1[2];
    vd03 = filtVars_1[3];
    vd04 = filtVars_1[4];
    vd05 = filtVars_1[5];
    vd10 = filtVars_2[0];
    vd11 = filtVars_2[1];
    vd12 = filtVars_2[2];
    vd13 = filtVars_2[3];
    vd14 = filtVars_2[4];
    vd15 = filtVars_2[5];


    /* IIR filtering for i/p block length */
    for (samp = 0; samp < count; samp++)
    {
        input1 = *in1++;//, *in2++);
        input2 = *in2++;

		//Channel 1&2 stage 1
        output01 = input1 + vd00;
        t1      = (((b11 + a11) * input1) + vd01) ;
        t0      = (a12 * vd00);
        vd00     =  ((a11 * vd00) + t1) ;
        vd01     = (((b12 + a12) * input1) + t0) ;

        //Channel 2 stage 1
        output11 = input2 + vd10;
	   t1      = (((b11 + a11) * input2) + vd11) ;
	   t0      = (a12 * vd10);
	   vd10     =  ((a11 * vd10) + t1) ;
	   vd11     = (((b12 + a12) * input2) + t0) ;

	    // Channel 1 stage 2
        output02 = (output01 + vd02);
        t3      = (((b21 + a21) * output01) + vd03); //(b21 + a21) * output1 + d3 ;
        t2      = (a22 * vd02);
        vd02     = ((a21 * vd02) + t3) ;
        vd03     = (((b22 + a22) * output01) + t2) ;
        // Channel 2 stage 2
        output12 = (output11 + vd12);
		t3      = (((b21 + a21) * output11) + vd13); //(b21 + a21) * output1 + d3 ;
		t2      = (a22 * vd12);
		vd12     = ((a21 * vd12) + t3) ;
		vd13     = (((b22 + a22) * output11) + t2) ;

		// Channel 1 stage 3
        output03 = (output02 + vd04);
        t5      = (((b31 + a31) * output02) + vd05) ;
        t4      = (a32 * vd04);
        vd04      = ((a31 * vd04) + t5) ;
        vd05      = (((b32 + a32) * output02) + t4) ;

        // Channel 2 stage 3
		output13 = (output12 + vd14);
		t5      = (((b31 + a31) * output12) + vd15) ;
		t4      = (a32 * vd14);
		vd14      = ((a31 * vd14) + t5) ;
		vd15      = (((b32 + a32) * output12) + t4) ;

		//Output samples per channel
        *out1++ = output03;
        *out2++ = output13;

    }

     /* Update state memory */
    filtVars_1[0] = (vd00);
    filtVars_1[1] = (vd01);
    filtVars_1[2] = (vd02);
    filtVars_1[3] = (vd03);
    filtVars_1[4] = (vd04);
    filtVars_1[5] = (vd05);

    filtVars_2[0] = (vd10);
    filtVars_2[1] = (vd11);
    filtVars_2[2] = (vd12);
    filtVars_2[3] = (vd13);
    filtVars_2[4] = (vd14);
    filtVars_2[5] = (vd15);

    return 0;
}

