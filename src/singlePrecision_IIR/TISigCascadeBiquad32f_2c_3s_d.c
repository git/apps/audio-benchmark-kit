/* ======================================================================== *
 * TISigLib -- TI Signal Processing Floating-Point Math Function Library    *
 *                                                                          *
 *                                                                          *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/   *
 *                                                                          *
 *                                                                          *
 *  Redistribution and use in source and binary forms, with or without      *
 *  modification, are permitted provided that the following conditions      *
 *  are met:                                                                *
 *                                                                          *
 *    Redistributions of source code must retain the above copyright        *
 *    notice, this list of conditions and the following disclaimer.         *
 *                                                                          *
 *    Redistributions in binary form must reproduce the above copyright     *
 *    notice, this list of conditions and the following disclaimer in the   *
 *    documentation and/or other materials provided with the                *
 *    distribution.                                                         *
 *                                                                          *
 *    Neither the name of Texas Instruments Incorporated nor the names of   *
 *    its contributors may be used to endorse or promote products derived   *
 *    from this software without specific prior written permission.         *
 *                                                                          *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     *
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR   *
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT    *
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,   *
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT        *
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,   *
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY   *
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT     *
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    *
 * ======================================================================== */

/* =========================================================================*/
/* TISigCascadeBiquadSP_2c_3s_d.c - single precision floating point sqrt    */
/* Author: Asheesh Bhardwaj										            */
/* =========================================================================*/
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include "c6x.h"
#include "TISigCascadeBiquad32f_2c_3s.h"

// SOC defines
#include <ti/drv/uart/UART.h>
#include <ti/drv/uart/UART_stdio.h>
#include "soc.h"
#include "Benchmark_log.h"
#include "ti/board/board.h"
#include "board_cfg.h"

#define	L2CFG	0x01840000
#define	MAR128	0x01848200

#pragma DATA_ALIGN(in_ptr1, 8);
#pragma DATA_ALIGN(in_ptr2, 8);
#pragma DATA_ALIGN(out_ptr1, 8);
#pragma DATA_ALIGN(out_ptr2, 8);
#pragma DATA_ALIGN(out_ptrRef1, 8);
#pragma DATA_ALIGN(out_ptrRef2, 8);


#define CHANNELS 2
#define NUM_STAGES 3
#define MAXITER 5

float in_ptr1[NY];
float in_ptr2[NY];

float out_ptr1[NY];
float out_ptr2[NY];

float out_ptrRef1[NY];
float out_ptrRef2[NY];

float  pCoef[CHANNELS*NUM_STAGES*2];
float  pVar0[NUM_STAGES*2] ;
float  pVar1[NUM_STAGES*2] ;
float  pVarRef0[NUM_STAGES*2] ;
float  pVarRef1[NUM_STAGES*2] ;
/* ======================================================================= */
/* Prototypes for utility functions                                        */
/* ======================================================================= */
void UTIL_fillRandSP(float *ptr_x, int N, float factor);
void UTIL_fillRandFilterSP(float *ptr_h, int N);

int tisigCascadeBiquad32f_2c_3skernel(CascadeBiquad_FilParam *pParam);
int tisigCascadeBiquad32f_2c_3skernel_cn(CascadeBiquad_FilParam *pParam);

void main(void) {
	
	CascadeBiquad_FilParam fparams;
	CascadeBiquad_FilParam fparamsRef;

	int i,j, iter;
	float pct_diff, max_pct_diff = 0;

    #ifndef IO_CONSOLE
	   Board_initCfg boardCfg;
	   
	#if defined(SOC_K2E) || defined(SOC_C6678)|| defined(SOC_K2H)|| defined(SOC_C6657)
    boardCfg = BOARD_INIT_MODULE_CLOCK |
               BOARD_INIT_UART_STDIO;
	#else
    boardCfg = BOARD_INIT_PINMUX_CONFIG |
               BOARD_INIT_MODULE_CLOCK  |
               BOARD_INIT_UART_STDIO;
	#endif
	   
       Board_init(boardCfg);
	#endif
   
   AUDIO_log(" /* ------------------------------------------------------------------  */\n");
   AUDIO_log(" /* Single Precision IIR Benchmarks multichannel cascade biquad filter  */\n");
   AUDIO_log(" /* ------------------------------------------------------------------  */\n");

	/* ------------------------------------------------------------------- */
	/* Compute the overhead of calling clock twice to get timing info      */
	/* ------------------------------------------------------------------- */
	clock_t t_overhead, t_start, t_stop, t_cn, t_opt;
	TSCL= 0,TSCH=0;
	t_start = _itoll(TSCH, TSCL);
	t_stop = _itoll(TSCH, TSCL);
	t_overhead = t_stop - t_start;

	fparams.pin1 = in_ptr1;
	fparams.pin2 = in_ptr2;
	fparams.pOut1 = out_ptr1;
	fparams.pOut2 = out_ptr2;
	fparamsRef.pOut1 = out_ptrRef1;
	fparamsRef.pOut2 = out_ptrRef2;

	for (iter=0; iter< MAXITER; iter++ ){
		memset(fparams.pin1, 0, sizeof(fparams.pin1));
		memset(fparams.pin2, 0, sizeof(fparams.pin2));
		memset(fparams.pOut1, 0, sizeof(fparams.pOut1));
		memset(fparams.pOut2, 0, sizeof(fparams.pOut2));
		memset(fparamsRef.pOut1, 0, sizeof(fparamsRef.pOut1));
		memset(fparamsRef.pOut2, 0, sizeof(fparamsRef.pOut2));

		fparams.pVar0 = pVar0;
		fparams.pVar1 = pVar1;
		fparams.pCoef = pCoef;

		UTIL_fillRandSP(fparams.pin1, NY, 10.0);
		UTIL_fillRandSP(fparams.pin2, NY, 20.0);
		UTIL_fillRandFilterSP(fparams.pVar0, NUM_STAGES*2);
		UTIL_fillRandFilterSP(fparams.pVar1, NUM_STAGES*2);
		UTIL_fillRandFilterSP(fparams.pCoef, NUM_STAGES*CHANNELS*2);

		fparamsRef.pin1 = fparams.pin1;
		fparamsRef.pin2 = fparams.pin2;
		fparamsRef.pVar0 = pVarRef0;
		fparamsRef.pVar1 = pVarRef1;
		fparamsRef.pCoef = fparams.pCoef;
		memcpy(fparamsRef.pVar0, fparams.pVar0, NUM_STAGES*2*sizeof(float) );
		memcpy(fparamsRef.pVar1, fparams.pVar1, NUM_STAGES*2*sizeof(float) );

		fparams.sampleCount = NY;
		fparamsRef.sampleCount = NY;

		t_start = _itoll(TSCH, TSCL);
		tisigCascadeBiquad32f_2c_3skernel_cn(&fparamsRef);
		t_stop  = _itoll(TSCH, TSCL);
		t_cn = (t_stop - t_start) - t_overhead;
		AUDIO_log("Iteration: %d\n", iter);
		AUDIO_log("\n\t2 channel 3 stage C66x NatC: %d\n", t_cn);

		t_start = _itoll(TSCH, TSCL);
		tisigCascadeBiquad32f_2c_3skernel(&fparams);
		t_stop  = _itoll(TSCH, TSCL);
		t_opt = (t_stop - t_start) - t_overhead;
		AUDIO_log("\n\t2 channel 3 stage C66x Intrinsic: %d\n", t_opt);
	}

/* --------------------------------------------------------------- */
/* compute percent difference and track max difference             */
/* --------------------------------------------------------------- */

	for(i=0; i<NY; i++) {
	  pct_diff = (out_ptrRef1[i] - out_ptr1[i]) / out_ptrRef1[i] * 100.0;
	  if (pct_diff < 0) pct_diff *= -1;
	  if (pct_diff > max_pct_diff) max_pct_diff = pct_diff;
	}
	if (max_pct_diff > 0.01)
		AUDIO_log("Result Failure  Channel 0 max_pct_diff = %f\n", max_pct_diff);
	else
		AUDIO_log("Result Successful  Channel 0\n");

	for(i=0; i<NY; i++) {
	  pct_diff = (out_ptrRef2[i] - out_ptr2[i]) / out_ptrRef2[i] * 100.0;
	  if (pct_diff < 0) pct_diff *= -1;
	  if (pct_diff > max_pct_diff) max_pct_diff = pct_diff;
	}
	if (max_pct_diff > 0.01)
		AUDIO_log("Result Failure  Channel 1 max_pct_diff = %f\n", max_pct_diff);
	else
		AUDIO_log("Result Successful  Channel 1\n");
	
	AUDIO_log("Test Passed\n");
	return;
}


void UTIL_fillRandSP(float *ptr_x, int N, float factor)
{
    float rand_midpoint = RAND_MAX / 2.0;
    int i;

    // fill array with floats in the range (-factor, factor)
    for (i = 0; i < N; i++)
        ptr_x[i] = ((rand() - rand_midpoint) / rand_midpoint) * factor;
}

void UTIL_fillRandFilterSP(float *ptr_h, int N)
{
    float frand_max = RAND_MAX, pair_sum = 2.0 / N;
    int i;

    // create a moving average filter of length N
    // filter taps must be non-negative and sum to 1
    // N must be an even number
    for (i = 0; i < N / 2; i++)
    {
        ptr_h[i] = (rand() / frand_max) * pair_sum;
        ptr_h[N - 1 - i] = pair_sum - ptr_h[i];
    }
}
