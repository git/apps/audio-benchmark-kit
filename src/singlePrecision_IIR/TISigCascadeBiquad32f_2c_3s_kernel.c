/* ======================================================================== *
 * TISigLib -- TI Signal Processing Floating-Point Math Function Library    *
 *                                                                          *
 *                                                                          *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/   *
 *                                                                          *
 *                                                                          *
 *  Redistribution and use in source and binary forms, with or without      *
 *  modification, are permitted provided that the following conditions      *
 *  are met:                                                                *
 *                                                                          *
 *    Redistributions of source code must retain the above copyright        *
 *    notice, this list of conditions and the following disclaimer.         *
 *                                                                          *
 *    Redistributions in binary form must reproduce the above copyright     *
 *    notice, this list of conditions and the following disclaimer in the   *
 *    documentation and/or other materials provided with the                *
 *    distribution.                                                         *
 *                                                                          *
 *    Neither the name of Texas Instruments Incorporated nor the names of   *
 *    its contributors may be used to endorse or promote products derived   *
 *    from this software without specific prior written permission.         *
 *                                                                          *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     *
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR   *
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT    *
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,   *
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT        *
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,   *
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY   *
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT     *
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    *
 * ======================================================================== */

/* ===========================================================================*/
/* tisigCascadeBiquadSP_2c_3s_kernel.c - single precision floating point sqrt */
/* Author: Asheesh Bhardwaj										              */
/* ===========================================================================*/
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include "c6x.h"
#include "c6x_vec.h"
#include "TISigCascadeBiquad32f_2c_3s.h"


int tisigCascadeBiquad32f_2c_3skernel(CascadeBiquad_FilParam *pParam)
{
    float *restrict in1, *restrict in2, *restrict out1, *restrict out2;
    float2 input1, output1, output2, output3;

    float *restrict filtCfs ; /* Coeff ptrs */
    float *restrict filtVars_1, *restrict filtVars_2; /* Filter var mem ptr */
    int count, samp; /* Loop counters */

    float b11, b12, a11, a12;  /* Cascade-1 coeffs   */
    float b21, b22, a21, a22;  /* Cascade-2 coeffs   */
    float b31, b32, a31, a32;  /* Cascade-3 coeffs   */
    float b11_a11, b12_a12, b21_a21, b22_a22, b31_a31, b32_a32;
    float2 vd0, vd1, vd2, vd3, vd4, vd5;

    float2 t0, t1, t2, t3, t4, t5;/* Filter temp regs  */
    float2 var_11, var_12;
    float2 var_21, var_22, var_31, var_32;
    float2 a11_a11, a21_a21, a12_a12, a22_a22, a32_a32, a31_a31;

    count = pParam->sampleCount; /* I/p sample block-length */


    in1 = (float *)pParam->pin1;
	in2 = (float *)pParam->pin2;

	out1 = (float *)pParam->pOut1;
	out2 = (float *)pParam->pOut2;


    filtVars_1 = (float *)pParam->pVar0; /* Get filter var ptr */
    filtVars_2 = (float *)pParam->pVar1; /* Get filter var ptr */
    filtCfs = (float *)pParam->pCoef; /* Feed-forward coeff. ptr */

    b11 = filtCfs[0];
    b12 = filtCfs[1];
    a11 = filtCfs[2];
    a12 = filtCfs[3];

    b21 = filtCfs[4];
    b22 = filtCfs[5];
    a21 = filtCfs[6];
    a22 = filtCfs[7];

    b31 = filtCfs[8];
    b32 = filtCfs[9];
    a31 = filtCfs[10];
    a32 = filtCfs[11];

    /* Get the filter states into corresponding regs */

    vd0= (float2)(filtVars_1[0], filtVars_2[0]);
    vd1= (float2)(filtVars_1[1], filtVars_2[1]);
    vd2= (float2)(filtVars_1[2], filtVars_2[2]);
    vd3= (float2)(filtVars_1[3], filtVars_2[3]);
    vd4= (float2)(filtVars_1[4], filtVars_2[4]);
    vd5= (float2)(filtVars_1[5], filtVars_2[5]);

    b11_a11 = b11 + a11;
    b12_a12 = b12 + a12;
    b21_a21 = b21 + a21;
    b22_a22 = b22 + a22;
    b31_a31 = b31 + a31;
    b32_a32 = b32 + a32;


    var_11 = (float2)(b11_a11, b11_a11);
    var_12 = (float2)(b12_a12, b12_a12);
    var_21 = (float2)(b21_a21, b21_a21);
    var_22 = (float2)(b22_a22, b22_a22);
    var_31 = (float2)(b31_a31, b31_a31);
    var_32 = (float2)(b32_a32, b32_a32);

    a11_a11 = (float2)(a11, a11);
    a12_a12 = (float2)(a12, a12);
    a21_a21 = (float2)(a21, a21);
    a22_a22 = (float2)(a22, a22);
    a31_a31 = (float2)(a31, a31);
    a32_a32 = (float2)(a32, a32);

    /* IIR filtering for i/p block length */
    for (samp = 0; samp < count; samp++)
    {
        input1 = (float2)(*in1++, *in2++);

		//Channel 1&2 stage 1
        output1 = input1 + vd0;
        t1      = ((var_11 * input1) + vd1) ;
        t0      = (a12_a12 * vd0);
        vd0     =  ((a11_a11 * vd0) + t1) ;
        vd1     = ((var_12 * input1) + t0) ;

	    // Channel 1&2 stage 2
        output2 = (output1 + vd2);
        t3      = ((var_21 * output1) + vd3); //(b21 + a21) * output1 + d3 ;
        t2      = (a22_a22 * vd2);
        vd2     = ((a21_a21 * vd2) + t3) ;
        vd3     = ((var_22 * output1) + t2) ;

		// Channel 1&2 stage 3
        output3 = (output2 + vd4);
        t5      = ((var_31 * output2) + vd5) ;
        t4      = (a32_a32 * vd4);
        vd4      = ((a31_a31 * vd4) + t5) ;
        vd5      = ((var_32 * output2) + t4) ;

        *out1++ = output3.lo;
        *out2++ = output3.hi;

    }

     /* Update state memory */
    filtVars_1[0] = (vd0.lo);
    filtVars_1[1] = (vd1.lo);
    filtVars_1[2] = (vd2.lo);
    filtVars_1[3] = (vd3.lo);
    filtVars_1[4] = (vd4.lo);
    filtVars_1[5] = (vd5.lo);

    filtVars_2[0] = (vd0.hi);
    filtVars_2[1] = (vd1.hi);
    filtVars_2[2] = (vd2.hi);
    filtVars_2[3] = (vd3.hi);
    filtVars_2[4] = (vd4.hi);
    filtVars_2[5] = (vd5.hi);

    return 0;
}



